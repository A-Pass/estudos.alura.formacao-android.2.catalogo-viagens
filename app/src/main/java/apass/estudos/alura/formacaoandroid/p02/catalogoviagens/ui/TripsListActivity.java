package apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.R;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.data.dao.TripDao;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.data.dao.TripDaoImpl;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.model.Trip;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ui.adapter.TripsListAdapter;

public class TripsListActivity extends AppCompatActivity {

    public static final String APPBAR_TITLE = "Viagens";

    private final List<Trip> trips = new TripDaoImpl().trips();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trips_list);

        setTitle(APPBAR_TITLE);

        setupTripsList();
    }

    private void setupTripsList() {
        ListView tripsList = findViewById(R.id.activity_trips_list_packages_list);

        tripsList.setAdapter(new TripsListAdapter(trips, this));

        tripsList.setOnItemClickListener((parent, view, position, id) -> {
            Trip selectedTrip = trips.get(position);
            Intent intent = new Intent(TripsListActivity.this, TripSummaryActivity.class);
            intent.putExtra(TripSummaryActivity.PARAM_TRIP, selectedTrip);
            startActivity(intent);
        });
    }
}