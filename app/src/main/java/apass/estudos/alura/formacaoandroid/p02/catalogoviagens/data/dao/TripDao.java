package apass.estudos.alura.formacaoandroid.p02.catalogoviagens.data.dao;

import java.util.List;

import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.model.Trip;

public interface TripDao {

    List<Trip> trips();
}
