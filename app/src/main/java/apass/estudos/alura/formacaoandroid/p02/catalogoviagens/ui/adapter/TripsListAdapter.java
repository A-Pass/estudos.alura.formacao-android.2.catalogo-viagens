package apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;

import java.util.List;

import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ui.BindTripView;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.R;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.model.Trip;

public class TripsListAdapter extends BaseAdapter {
    private final List<Trip> trips;
    private final Context context;

    public TripsListAdapter(List<Trip> trips, Context context) {
        this.trips = trips;
        this.context = context;
    }

    @Override
    public int getCount() {
        return trips.size();
    }

    @Override
    public Trip getItem(int index) {
        return trips.get(index);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        @SuppressLint("ViewHolder") View inflate = LayoutInflater.from(context)
                .inflate(R.layout.item_trip_package, viewGroup, false);

        Trip trip = trips.get(i);

        bind(inflate, trip);

        return inflate;
    }

    private void bind(View view, Trip trip) {
        BindTripView bvw = new BindTripView(view, trip);
        bvw.bindImage(R.id.item_trip_package_image);
        bvw.bindCity(R.id.trip_package_city);
        bvw.bindNumberOfDays(R.id.trip_package_number_of_days);
        bvw.bindPrice(R.id.trip_package_price);
    }
}
