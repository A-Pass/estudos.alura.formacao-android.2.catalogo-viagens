package apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ui;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.IdRes;

import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.model.Trip;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ultility.CurrencyUtility;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ultility.ResourceUtility;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ultility.DateUtility;

public class BindTripView {
    private final View view;
    private final Trip trip;

    public BindTripView(View view, Trip trip) {
        this.view = view;
        this.trip = trip;
    }

    public BindTripView bindCity(@IdRes int viewId) {
        ((TextView)view.findViewById(viewId)).setText(trip.getCity());
        return this;
    }

    public BindTripView bindImage(@IdRes int viewId) {
        Drawable drw = ResourceUtility.getDrawableByName(view.getContext(), trip.getImage());
        ImageView imgVw = view.findViewById(viewId);
        imgVw.setImageDrawable(drw);
        return this;
    }

    public BindTripView bindNumberOfDays(@IdRes int viewId) {
        ((TextView) view.findViewById(viewId)).setText(DateUtility.Days.format(trip.getNumberOfDays()));
        return this;
    }

    public BindTripView bindPrice(@IdRes int viewId) {
        ((TextView) view.findViewById(viewId)).setText(CurrencyUtility.formatToBrazilian(trip.getPrice()));
        return this;
    }

    public BindTripView bindPeriod(@IdRes int viewId) {
        ((TextView) view.findViewById(viewId)).setText(
                DateUtility.Period
                        .format(trip.getStartDate(), trip.getEndDate()));
        return this;
    }
}
