package apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.R;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.model.Trip;

public class TripSummaryActivity extends AppCompatActivity {

    public static final String PARAM_TRIP = "PARAM_TRIP";

    public static final String APPBAR_TITLE = "Resumo Viagem";

    private Trip trip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_summary);

        setTitle(APPBAR_TITLE);

        bindArgs();
        bindView();
    }

    private void bindArgs() {
        if (getIntent().hasExtra(PARAM_TRIP)) {
            trip = (Trip) getIntent().getSerializableExtra(PARAM_TRIP);
        }
    }

    private void bindView() {
        if (trip != null) {
            BindTripView bvw = new BindTripView(getWindow().getDecorView(), trip);
            bvw.bindImage(R.id.activity_trip_summary_city_img);
            bvw.bindCity(R.id.activity_trip_summary_city_txt);
            bvw.bindNumberOfDays(R.id.activity_trip_summary_number_of_days_txt);
            bvw.bindPrice(R.id.activity_trip_summary_price_txt);
            bvw.bindPeriod(R.id.activity_trip_summary_period_txt);

            ((Button) findViewById(R.id.activity_trip_summary_payment_btn)).setOnClickListener(v -> {
                Intent intent = new Intent(this, TripPaymentActivity.class);
                intent.putExtra(TripSummaryActivity.PARAM_TRIP, trip);
                startActivity(intent);
            });
        }
    }
}
