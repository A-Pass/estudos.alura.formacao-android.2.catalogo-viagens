package apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ultility;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Locale;

public final class CurrencyUtility {

    private static final String LANGUAGE_PATTER = "pt";
    private static final String COUNTRY_PATTER = "br";

    public static String formatToBrazilian(BigDecimal valor) {
        return DecimalFormat.getCurrencyInstance(new Locale(LANGUAGE_PATTER, COUNTRY_PATTER)).format(valor);
    }
}
