package apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.text.NumberFormat;

import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.R;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.data.dao.TripDaoImpl;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.model.Trip;

public class TripPaymentActivity extends AppCompatActivity {

    public static final String PARAM_TRIP = "PARAM_TRIP";
    public static final String APPBAR_TITLE = "Pagamento";
    private Trip trip;

    private void bindArgs() {
        if (getIntent().hasExtra(PARAM_TRIP)) {
            trip = (Trip) getIntent().getSerializableExtra(PARAM_TRIP);
        }
    }

    private void bindView() {
        if (trip != null) {
            ((TextView) findViewById(R.id.activity_trip_payment_purchase_price_value_txt))
                    .setText(NumberFormat.getCurrencyInstance().format(trip.getPrice()));

            ((Button) findViewById(R.id.activity_trip_payment_finish_purchase_btn))
                    .setOnClickListener(v -> toTripPurchaseSummary());
        }
    }

    private void toTripPurchaseSummary() {
        Intent intent = new Intent(this, TripPurchaseSummaryActivity.class);
        intent.putExtra(TripPurchaseSummaryActivity.PARAM_TRIP, trip);
        startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_payment);

        setTitle(APPBAR_TITLE);

        bindArgs();
        bindView();
    }
}
