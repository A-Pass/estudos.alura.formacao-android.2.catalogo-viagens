package apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.R;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.data.dao.TripDao;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.data.dao.TripDaoImpl;
import apass.estudos.alura.formacaoandroid.p02.catalogoviagens.model.Trip;

public class TripPurchaseSummaryActivity extends AppCompatActivity {

    public static final String PARAM_TRIP = "PARAM_TRIP";

    private static final String APPBAR_TITLE = "Resumo da compra";
    private Trip trip;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(APPBAR_TITLE);
        setContentView(R.layout.activity_trip_purchase_summary);

        bindArgs();
        bindView();
    }

    private void bindArgs() {
        if (getIntent().hasExtra(PARAM_TRIP)) {
            trip = (Trip) getIntent().getSerializableExtra(PARAM_TRIP);
        }
    }

    private void bindView() {
        if (trip != null) {
            new BindTripView(getWindow().getDecorView(), trip)
                    .bindCity(R.id.activity_trip_purchase_summary_city_txt)
                    .bindImage(R.id.activity_trip_purchase_summary_city_img)
                    .bindPeriod(R.id.activity_trip_purchase_summary_period_txt)
                    .bindPrice(R.id.activity_trip_purchase_summary_price_txt);
        }
    }
}
