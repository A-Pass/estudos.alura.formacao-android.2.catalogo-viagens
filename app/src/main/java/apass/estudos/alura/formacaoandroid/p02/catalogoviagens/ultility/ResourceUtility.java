package apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ultility;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;

public final class ResourceUtility {

    public static Drawable getDrawableByName(@NonNull Context context, String name) {
        String TYPE_DRAWABLE = "drawable";
        return context.getResources().getDrawable(
                context.getResources().getIdentifier(name, TYPE_DRAWABLE, context.getPackageName()));
    }
}
