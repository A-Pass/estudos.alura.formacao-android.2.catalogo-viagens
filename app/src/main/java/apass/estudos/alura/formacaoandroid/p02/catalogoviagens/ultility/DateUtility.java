package apass.estudos.alura.formacaoandroid.p02.catalogoviagens.ultility;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class DateUtility {


    public final static class Days {
        private static final String DAY_SINGLE = "dia";
        private static final String DIA_PLURAL = " " + DAY_SINGLE + "s";

        public static String format(Integer dias) {
            return dias + (dias > 1 ? DIA_PLURAL : " " + DAY_SINGLE);
        }
    }

    public final static class Period {
        private static final String DATE_PATTER = "dd/MM";

        public static String format(Calendar start, Calendar end) {
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTER);
            return sdf.format(start.getTime()) +
                    " - " +
                    sdf.format(end.getTime()) +
                    " de " +
                    end.get(Calendar.YEAR);
        }
    }
}
