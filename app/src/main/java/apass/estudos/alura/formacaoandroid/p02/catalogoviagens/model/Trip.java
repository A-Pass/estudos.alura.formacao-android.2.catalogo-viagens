package apass.estudos.alura.formacaoandroid.p02.catalogoviagens.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

public class Trip implements Serializable {

    private final String city;
    private final String image;
    private final Integer numberOfDays;
    private final BigDecimal price;

    public Trip(String city, String image, Integer numberOfDays, BigDecimal price) {
        this.city = city;
        this.image = image;
        this.numberOfDays = numberOfDays;
        this.price = price;
    }


    public String getCity() {
        return city;
    }

    public String getImage() {
        return image;
    }

    public Integer getNumberOfDays() {
        return numberOfDays;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Calendar getStartDate() {
        return Calendar.getInstance();
    }

    public Calendar getEndDate() {
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.DATE, numberOfDays);
        return endDate;
    }
}
